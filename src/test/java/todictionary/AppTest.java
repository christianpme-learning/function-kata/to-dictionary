package todictionary;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Dictionary;
import java.util.Hashtable;

import org.junit.Test;

public class AppTest {
    @Test
    public void toDictionaryTest() {
        final App app = new App();
        Dictionary<String, String> actual = null;
        try {
            actual = app.toDictionary("a=1;b=2;c=3");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        final Dictionary<String, String> expected = new Hashtable<String, String>();
        expected.put("a", "1");
        expected.put("b", "2");
        expected.put("c", "3");

        assertTrue(expected.equals(actual));
    }

    @Test
    public void toDictionaryUpdateValueTest() {
        final App app = new App();
        Dictionary<String, String> actual = null;
        try {
            actual = app.toDictionary("a=1;a=2");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        final Dictionary<String, String> expected = new Hashtable<String, String>();
        expected.put("a", "1");
        expected.put("a", "2");

        assertTrue(expected.equals(actual));
    }

    @Test
    public void toDictionaryIgnoreSeveralSemicolonsTest() {
        final App app = new App();
        Dictionary<String, String> actual = null;
        try {
            actual = app.toDictionary("a=1;;b=2");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        final Dictionary<String, String> expected = new Hashtable<String, String>();
        expected.put("a", "1");
        expected.put("b", "2");

        assertTrue(expected.equals(actual));
    }

    @Test
    public void toDictionaryEmptyValueTest() {
        final App app = new App();
        Dictionary<String, String> actual = null;
        try {
            actual = app.toDictionary("a=");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        final Dictionary<String, String> expected = new Hashtable<String, String>();
        expected.put("a", "");

        assertTrue(expected.equals(actual));
    }

    @Test
    public void toDictionaryEmptyKeyTest() {
        try {
            final App app = new App();
            app.toDictionary("=1");
            fail("Expected an Exception to be thrown");
        } catch (final Exception e) {
            e.printStackTrace();
            assertTrue("empty key".equals(e.getMessage()));
        }
    }

    @Test
    public void toDictionaryEmptyTest() {
        final App app = new App();
        Dictionary<String, String> actual = null;
        try {
            actual = app.toDictionary("");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        final Dictionary<String, String> expected = new Hashtable<String, String>();

        assertTrue(expected.equals(actual));
    }

    @Test
    public void toDictionaryHandleSeveralEqualsTest() {
        final App app = new App();
        Dictionary<String, String> actual = null;
        try {
            actual = app.toDictionary("a==1");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        final Dictionary<String, String> expected = new Hashtable<String, String>();
        expected.put("a", "=1");
        
        assertTrue(expected.equals(actual));
    }
}
