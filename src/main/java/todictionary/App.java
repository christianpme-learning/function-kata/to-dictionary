package todictionary;

import java.util.Dictionary;
import java.util.Hashtable;

public class App 
{
    public Dictionary<String, String> toDictionary(final String string) throws Exception {
        final Dictionary<String, String> dictionary = new Hashtable<>();
        final String[] elements = string.split(";");
        for (final String element : elements) {
            if (!element.isEmpty()) {
                final String[] pair = parseToPair(element);

                dictionary.put(pair[0], pair[1]);
            }
        }
        return dictionary;
    }

    public String[] parseToPair(final String element) throws Exception {
        final String[] pair = new String[] { "", "" };
        boolean unequal = true;

        if (element.charAt(0) == '=') {
            throw new Exception("empty key");
        }

        for (final Character character : element.toCharArray()) {
            if(unequal && character.equals('=')){
                unequal = false;
            }
            else{
                if(unequal){
                    pair[0] += character;
                }
                else{
                    pair[1] += character;
                }
            }
        }
        return pair;
    }
}


